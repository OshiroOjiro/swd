Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'users#index'
  get 'top/login'
  post 'top/login'
  get 'top/logout'
  resources :users, only: [:index, :new, :create, :destroy]
end
